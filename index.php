<?php

# Design of a URL for peppercarrot2021:
# =====================================
# https://wwww.peppercarrot.com/{$lang}/{$mode}/{$content}__{$option}.html
# ...or same URL without URL rewrite:
# https://www.peppercarrot.com/index.php?lang=en?mode=webcomic?content=ep01_Potion-of-Flight?option=hd-transcript
# Every URL starting by two small case letter and a slash (eg "en/") will trigger website engine to process the URL.
# $mode: loads a specific engine to read a type content (eg. page|wiki|webcomic)
# $option is cut after two underscore delimiter "__" and word in it are split into an array by "-" delimiter.
# (see .htaccess to see how rewrite rule distribute them)

# Design for language detection:
# ==============================
# If the URL contains a ISO language, apply the language.
# If a cookie exists, extract and apply language.
# If no cookie found or URL, apply webbrowser default language.
# If no language applied, fallback to English.

# Root of the files of the website is equal to full path to directory where this index.php is.
$file_root = dirname(__FILE__).'/';

# Init:
$version = "202107e";
$starttime = microtime(true);
$lang = "";
$mode = "";
$content = "";
$option = "";
# Trigger a 404 if equal to 1
$you_shall_not_pass = 0;
$not_found = 0;
$test = 0;
# Series title and author info
$credits = '_Pepper-and-Carrot_by-David-Revoy_';

# Config (see configure.php.new)
if (file_exists($file_root.'configure.php')) {
  include_once($file_root.'configure.php');
} else {
  http_response_code(404);
  echo "<pre><strong>Pepper&amp;Carrot website error: configure.php not found.</strong> <br/>";
  echo "Copy configure.php.new to configure.php and make any changes you need for your server setup.</pre>";
  die();
}

# Loading variable from URL
if (isset($_GET['lang'])) {
  $lang = htmlspecialchars($_GET['lang']);
}
if (isset($_GET['mode'])) {
  $mode = htmlspecialchars($_GET['mode']);
}
if (isset($_GET['content'])) {
  $content = htmlspecialchars($_GET['content']);
}
if (isset($_GET['option'])) {
  $option = htmlspecialchars($_GET['option']);
}

# Security: sanitize data the page receives from the URL
$lang = filter_var($lang, FILTER_SANITIZE_URL);
$lang = preg_replace('/[^a-z\._-]/', '', $lang);
$mode = filter_var($mode, FILTER_SANITIZE_URL);
$mode = preg_replace('/[^A-Za-z0-9\._-]/', '', $mode);
$content = filter_var($content, FILTER_SANITIZE_URL);
$content = preg_replace('/[^A-Za-z0-9\._-]/', '', $content);
$option = filter_var($option, FILTER_SANITIZE_URL);
$option = preg_replace('/[^A-Za-z0-9\._\~-]/', '', $option);


# Manage $lang:
# -------------
# Load our favorite language from saved cookie 'lang', if it exists.
# (Note: this type of cookie is totally allowed by RGPD without requiring user consent).
if(isset($_COOKIE['lang'])) {
  $favlang = $_COOKIE['lang'];
} else {
  $favlang = '';
}

# Rules for undefined language in URL (homepage)
if (empty($lang)) {
  # Check if we have a cookie:
  if(isset($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
  } else {
    # Guess the language of the browser:
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  }
}

# Default mode if undefined
if (empty($mode)) {
  $mode = "homepage";
};

# If still empty after that, hardcode English
if (empty($lang)) {
  $lang = "en";
}

# Load Gettext (with a hack):
# In order to load arbitrary *.mo file on disk while still using vanilla Gettext, we need a hack.
# All $lang.mo files are stored in 'locale/en_US.utf8/LC_MESSAGES/' folder.
# All $lang.po files are stored in 'po' folder.
# Without this hack, language validation by gettext would depend on locale installed at a server level.
# (eg. no Lojban, requirement to get permission to install and activate more than 50 locale on a server, etc...)
setlocale(LC_ALL, 'en_US.utf8');
bindtextdomain ("$lang", "locale");
textdomain ("$lang");

# Manage $options in URL:
# -----------------------
# Explode option into an array (eg. "hd-transcript" > ["hd", "transcript"])
$option = explode("-", rtrim($option, "/"));

# Get all options from the URL and input $keyword among them.
function _newoption($keyword){
  global $option;
  # Create a copy of the array to preserve the original
  $newoption = $option;
  # We found the keyword
  if (in_array($keyword,$newoption)) {
    # Remove the option, so it create a toggle switch
    $target = array_search($keyword,$newoption);
    if($target !== FALSE){
        unset($newoption[$target]);
    }
  } else {
    # Insert the option
    array_push($newoption,$keyword);
  }
  # Remove empty item in the array
  $newoption = array_filter($newoption);
  # Build our URL
  if(empty($newoption)) {
    $fullnewoptions = '';
  } else {
    $listofnewoptions = implode('-', $newoption);
    $fullnewoptions = '__'.$listofnewoptions;
  }
  return $fullnewoptions;
}

# Manage cookies:
# ---------------
# Save a cookie of $lang (if previous page sent 'c' option, a feature of 'save button', in mod-header.php)
if ($mode == "setup") {
  setcookie("lang", $lang, time() + (86400 * 365), "/"); # 86400 = 1 day
  $target = array_search("c",$option);
  if($target !== FALSE){
    unset($option[$target]);
  }
  #Refresh faster this variable for debug and menu:
  $favlang = $lang;
}

# Manage future generation of links:
# ----------------------------------
$extension = '';
# If on peppercarrot.com, make links like peppercarrot.com/fr/, this pattern save a cookie.
if (!empty($mode)) {
  $printmode = ''.$mode.'/';
  $printcontent = '';
  $setoption = '';
}
# If on peppercarrot.com, hide engine (don't show peppercarrot.com/fr/homepage URL)
if ($mode == "homepage") {
  $printmode = '';
  $printcontent = '';
  $setoption = '';
}
# If on a page like peppercarrot.com/fr/mode/content_option1-2-3.html, plug a 'c' option to save a cookie.
if (!empty($content)) {
  $printcontent = $content;
  $extension = '.html';
  $setoption = _newoption("");
}

# Load databases and image tool:
# ------------------------------
include($file_root.'core/lib-database.php');
include($file_root.'core/lib-navigation.php');
include($file_root.'core/lib-image.php');


# Redirections:
# -------------
# Compatibility with legacy URLs:
# Check if $mode matches a legacy URL, and extract episode number at same time on $result
if (preg_match('/article[0-9][0-9][0-9]episode-([0-9]+)[a-z-]*/', $mode, $results)) {
  # Substract 1 from the result, because array of episode start at zero for ep01.
  $episode_requested = $results[1] - 1;
  $redirection = $root.'/'.$lang.'/webcomic/'.$episodes_list[$episode_requested].'.html';
  header('Location: '.$redirection.'', true, 301);
}
# Send to webcomic mode every obvious failed attempt to type url
if (preg_match('/webcomic*ep([0-9]+)[a-z-]*/', $mode, $results)) {
  # Substract 1 from the result, because array of episode start at zero for ep01.
  $episode_requested = $results[1] - 1;
  $redirection = $root.'/'.$lang.'/webcomic/'.$episodes_list[$episode_requested].'.html';
  header('Location: '.$redirection.'', true, 301);
}

# Change variable depending of $mode:
# -----------------------------------
if ($mode == "webcomic" ) {
  # Copy $content (extracted from URL on index.php) to our new $epdirectory
  $epdirectory = $content;
  # Test if $epdirectory $content matches the pattern of an episode
  # clean $content__$option on the fly by passing only the matched result (trim trailing '_', $option, .html)
  if (preg_match('/([a-z\-]+[0-9]+_[^_]+)/', $epdirectory, $matches)) {
    # Select the match: an 'episode directory'-like string
    $epdirectory = $matches[1];
    # Extract episode number to work later with array stored in database
    # Substract 1, because episode array starts at zero (eg. [0] => ep01_Potion-of-Flight [1] => ep02_Rainbow-potions))
    if (preg_match('/[a-z-]+([0-9]+)[a-z-]*/', $epdirectory, $results)) {
      $episode_number = $results[1] - 1;
    }
    # Test if the episode directory exists.
    if(is_dir(''.$sources.'/'.$epdirectory)) {
      # Success: URL request a valid episode.
      # Load the translation title (from titles.json file stored in episode, extracted by renderfarm)
      $json_titles = json_decode(file_get_contents(''.$root.'/'.$sources.'/'.$epdirectory.'/hi-res/titles.json'));
      if(isset($json_titles->{$lang})) {
        $header_title = $json_titles->{$lang};
      }
      # If the episode is not translated in the target language, the field will be empty and lead to a 404. We need to fill it in English.
      if (empty($header_title)) {
        $header_title = $json_titles->{'en'};
      }
      # Social media thumbnail, a picture that display when the URL is cited.
      # Number of episode is incremented +1 because this is real number (not array starting by 0)
      $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/'.$epdirectory.'/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E'.($episode_number + 1).'.jpg';
    } else {
      # Directory doesn't exist, let's try to fix it based on number
      if (!empty($episodes_list[$episode_number])){
        # Format ep6 to ep06 (leading 0, always two digits)
        $episode_number = sprintf('%01s', $episode_number);
        $redirection_episode = $root.'/'.$lang.'/webcomic/'.$episodes_list[$episode_number].'.html';
        header('Location: '.$redirection_episode.'', true, 301);
      } else {
        # We tried everything, the episode requested probably never existed.
        $not_found = 1;
      }
    }
  }

} else if ($mode == "viewer" ) {

# An exception for viewer to build a better thumbnail for the 'meta property="og:image"'
# when someone share a viewer link on social medias
  # Get the $mode (folder name in 0_sources/0ther)
  # if we detect a leading '-src' in the string, switch to display source mode and delete the indicator.
  $viewer_mode = 'regular';
  $content_for_url = $content.'-src';
  $source_button_class = '';
  if (preg_match('/[a-z-]+(-src)/', $content, $results)) {
    $content = str_replace($results[1], '', $content);
    $viewer_mode = 'show-sources';
    $content_for_url = $content;
    $source_button_class = ' active';
  }
  # Get picture from $option in URL (picture filename without extension)
  $option_get = implode("-", $option);
  $pattern = ''.$sources.'/0ther/'.$content.'/low-res/'.$option_get.'.jpg';
  # Build a title from the filename
  # Extract attribution for the author
  preg_match('/.*([-_]by[-_].*)/', $option_get, $attribution_part);
  $attribution = str_replace('-', ' ', $attribution_part[1]);
  $attribution = str_replace('by', '', $attribution);
  $attribution = str_replace('_', ' ', $attribution);
  $attribution = str_replace('  ', '', $attribution);
  # Extract the Title
  $title = str_replace($attribution_part[1], '', $option_get);
  $title = substr($title, 11);
  $title = str_replace('_', ' ', $title);
  $title = str_replace('-', ' ', $title);
  $title = ucfirst($title);
  # Generate the string for the browser's windows in the art slideshow viewer
  # %1$s = artwork title, %2$s = author
  $header_title = sprintf(_('%1$s by %2$s'), $title, $attribution).' – '._('Viewer');
  # Define the thumbnail for social medias
  $social_media_thumbnail_URL = ''.$root.'/'.$pattern.'';

} else {
  # All other mode are pages, a bit more static.
  # Send to social media a generic cover
  $social_media_thumbnail_URL = ''.$root.'/'.$sources.'/0ther/artworks/low-res/2016-11-14_vertical-cover-book-two_screen_by-David-Revoy.jpg';
  # In other case, redirect mode to better Gettext translated labels
  if ($content == "philosophy")   { $header_title = ''._("Philosophy"); };
  if ($mode == "artworks")        { $header_title = ''._("Artworks"); };
  if ($mode == "wiki")            { $header_title = $content.' - '._("Wiki").''; };
  if ($mode == "documentation")   { $header_title = $content.' - '._("Documentation").''; };
  if ($mode == "homepage")        { $header_title = _("Homepage"); };
  if ($mode == "files")           { $header_title = _("Files"); };
  if ($mode == "webcomics")       { $header_title = _("Webcomics"); };
  if ($mode == "goodies")         { $header_title = _("Goodies"); };
  if ($mode == "contribute")      { $header_title = _("Contribute"); };
  if ($mode == "fan-art")         { $header_title = _("Fan-art"); };
  if ($mode == "wallpapers")      { $header_title = _("Wallpapers"); };
  if ($mode == "setup")           { $header_title = _("Website setup"); };
  if ($mode == "about")           { $header_title = _("About"); };
  if ($mode == "support")         { $header_title = _("Support"); };
  if ($mode == "license")         { $header_title = _("License"); };
  if ($mode == "chat")            { $header_title = _("Chat rooms"); };
  if ($mode == "tos")             { $header_title = _("Terms of Services and Privacy"); };
  if ($mode == "xyz")             { $header_title = "XYZ"; };
}

# Define a translation table for all directories in 0_sources/0ther
$other_directories = array(
  'artworks'          =>    ''._("Artworks").'',
  'book-publishing'   =>    ''._("Book-publishing").'',
  'comissions'        =>    ''._("Commissions").'',
  'eshop'             =>    ''._("Shop").'',
  'fan-art'           =>    ''._("Fan-art").'',
  'framasoft'         =>    ''._("Framasoft").'',
  'misc'              =>    ''._("Misc").'',
  'press'             =>    ''._("Press").'',
  'references'        =>    ''._("References").'',
  'sketchbook'        =>    ''._("Sketchbook").'',
  'vector'            =>    ''._("Vector").'',
  'wallpapers'        =>    ''._("Wallpapers").'',
  'wiki'              =>    ''._("Wiki").''
);

# Load the 404 error mode
if (empty($header_title)) {
  $header_title = _("Not found:").' '.$mode.'/'.$content.'';
  $you_shall_not_pass = 1;
}

# Exclude $mode request starting by lib- (Libraries) or mod- (Modules)
# These pages are not supposed to be run by themselves.
if (false !== strpos($mode, 'mod-')) {
  $you_shall_not_pass = 1;
}
if (false !== strpos($mode, 'lib-')) {
  $you_shall_not_pass = 1;
}

###########################
# START BUILDING THE PAGE #
###########################

# Build the header of the html output page:
include($file_root.'core/mod-header.php');

# Build the content depending on $mode.
$filemode = $file_root.'core/'.$mode.'.php';
if (file_exists($filemode) AND $you_shall_not_pass !== 1 ) {
  # Include the main content
  include($filemode);
} else {
  # Fallback on a 404 page
  include($file_root.'core/404.php');
}

# Debug floating box with variable (change $debugmode in index.php at root to activate it)
if ($debugmode > 0.9){
  echo ''."\n";
  echo '<div style="color: #fff; font: 13px monospace; background: #000; position:fixed; bottom: 0px; right: 0px; width: 100%; height: 18px; opacity: 0.7; padding-top:3px;">'."\n";
  echo '  <center>'."\n";
  echo '<b>ALPHA website: work in progress, version '.$version.'</b>'."\n";
  $endtime = microtime(true);
  printf("| Page loaded in: %f seconds", $endtime - $starttime );
  echo '|| <span>'."\n";
  echo '$lang: '.$lang."\n";
  echo '| favlang: '.$favlang."\n";
  if (isset($_COOKIE) AND isset($_COOKIE['lang'])) {
    echo '| Cookie: '.$_COOKIE['lang']."\n";
  }
  echo '</span> || <span>'."\n";
  echo '$mode: '.$mode."\n";
  echo '| $content: '.$content."\n";
  echo '| $option: '._newoption('')."\n";
  echo '</span>'."\n";
  echo '| $test: '.$test."\n";
  echo '    <br/>'."\n";
  echo '  </center>'."\n";
  echo '</div>'."\n";
}

# Build the footer of the html output page:
include($file_root.'core/mod-footer.php');
?>

