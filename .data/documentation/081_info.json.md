# The credits: info.json

`info.json` files contain information about an episode or about a translation.

The JSON format is very strict regarding commas, so we also have an automated script available for testing your files.
If you have Python available, you can run it by calling
`python .ci/validate_json.py` or `python3 .ci/validate_json.py`, depending on your environment.
The check is also performed by our [CI pipelines](https://framagit.org/peppercarrot/webcomics/pipelines),
so don't worry if you can't run it yourself - we can fix it up for you.

## Translation info

Each `<episode/lang/<language>` folder should contain an `info.json` file with translator credits in it.
Here's an example:

```json
{
   "credits": {
      "translation": [
         "Nartance"
      ],
      "proofreading": [
         "CalimeroTeknik",
         "Valvin"
      ],
      "contribution": [
         "David Revoy <https://framagit.org/Deevad>"
      ]
   }
}
```

### Available Keys

* **translation** contains everybody who worked on the original translation
* **proofreading** is for linguistic fixes and improvements
* **contribution** is for layout and technical fixes, title graphics etc.

### Notes

* You only need to fill out the sections that you are using - you can delete the rest.
* If you see `"translation": [ "original version" ]` it means the episode was written into this language. 
* Links are optional. You can use an `http(s)://` or `mailto:` link, they need to be framed by superior/inferior symbols like this: `<https://framagit.org/Deevad>`.
* If you are doing a lot of SVG improvements across many episodes and language; it's easier to add yourself to `project-global-credits.json` at the root of the repository. It contains a `svg-database` for the heroes ready to work on the maintenance of the enormous database.
* We also have a [template file](https://framagit.org/peppercarrot/webcomics/-/blob/master/.ci/template_lang_info.json) available to help you get started.
 

## Episode info

Each episode folder should contain an `info.json` file too. This file contains
a lot more information than the translation info.
We also have templates available for these, one containing
[all permitted keys](https://framagit.org/peppercarrot/webcomics/-/blob/master/.ci/template_info.json)
and one containing only the
[mandatory keys](https://framagit.org/peppercarrot/webcomics/-/blob/master/.ci/template_info_mandatory.json).

This files is not really made to be changed by translator. This file should remain more or less the same after the episode creation.
