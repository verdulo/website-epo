<?php

# Lib-gallery.php
# -------------
# A loop to display thumbnails of $content (a folder in 0_sources/0ther)

function _gallery($content) {
  global $sources;
  global $lang;
  global $root;
  # Browser mode
  # Content: list the files
  if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
    $all_thumbnails = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
    rsort($all_thumbnails);
    foreach ($all_thumbnails as $thumb) {
      $thumb_filename = basename($thumb);
      $thumb_fullpath = dirname($thumb);
      $thumb_option_link = str_replace('.jpg', '', $thumb_filename);
      $cover_description = $thumb_option_link; #TODO better title/alt
      echo '      <li id="'.$thumb_option_link.'">'."\n";
      echo '        <a href="'.$root.'/'.$lang.'/viewer/'.$content.'__'.$thumb_option_link.'.html">'."\n";
      echo '          ';
        # Generated: all 400px height (and 900px bounding box for width). Good compromise to make ~200px thumb for large monitor, and big 400px thumb for phones.
        _img($sources.'/0ther/'.$content.'/low-res/'.$thumb_filename, $cover_description, 450, 320, 50);
        # Direct renderfarm thumb, for testing
        #echo '  <img src="'.$root.'/'.$sources.'/0ther/'.$content.'/low-res/thumb-res/'.$thumb_filename.'" title="'.$cover_description.'" alt=""/>';
      echo ''."\n";
      echo '        </a>'."\n";
      echo '      </li>'."\n";
    }
  echo '      <!-- Empty li tag (workaround) -->'."\n";
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  echo '      <li></li>'."\n";
  }
}

function _gallery_number($content) {
  global $sources;
  global $lang;
  global $root;
  $counter = 0;
  if(is_dir($sources.'/0ther/'.$content.'/low-res/')) {
    $all_thumbnails = glob($sources.'/0ther/'.$content.'/low-res/*.jpg');
    $counter = count($all_thumbnails);
  }
  return $counter;
}

?>
