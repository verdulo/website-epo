<?php

# Lib-navigation.php
# -------------
# Creates a navigation menu out of thow variable and an array.
# $current_page: a string that is similar to $content in the URL, this is where the user is.
# $full_array: the array of values similar to $current page; the full list of previous and next pages.
# $mode: to know how to rebuild URL.
#
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher


# Check if we can write in cache/ before starting.

function _navigation($current_page, $full_array, $input_mode) {

  global $root;
  global $sources;
  global $lang;
  global $mode;
  global $content;

  # Regular type of URL
  $url_pattern = 0;
  # Irregular type of URL
  if ( $mode == 'viewer' ){
    $url_pattern = 1;
  }
  if ( $mode == 'viewer-src' ){
    $url_pattern = 1;
  }

  if ( $input_mode == 'fan-art' ){
    $url_pattern = 1;
  }

  $episode_index = $full_array;
  $current = array_search($current_page, $episode_index);
  if(isset($episode_index[$current-1])) {
    $prev_episode = $episode_index[$current-1];
    $classprev = '';
    if ( $url_pattern == 0 ){
      $linkprev = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$prev_episode.''._newoption('').'.html';
    } else {
      $linkprev = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$content.'__'.$prev_episode.'.html';
    }
  } else {
    $prev_episode = $episode_index[$current];
    $classprev = 'off';
    $linkprev = '';
  }
  if(isset($episode_index[$current+1])) {
    $next_episode = $episode_index[$current+1];
    $classnext = '';
    if ( $url_pattern == 0 ){
      $linknext = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$next_episode.''._newoption('').'.html';
    } else {
      $linknext = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$content.'__'.$next_episode.'.html';
    }
  } else {
    $next_episode = $episode_index[$current];
    $classnext = 'off';
    $linknext = '';
  }
  $first_episode = $episode_index[0];
  $last_episode = end($episode_index);

  if ( $current_page == $first_episode ){
    $classfirst = 'off';
    $linkfirst = '';
  } else {
    $classfirst = '';
    if ( $url_pattern == 0 ){
      $linkfirst = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$first_episode.''._newoption('').'.html';
    } else {
      $linkfirst = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$content.'__'.$first_episode.'.html';
    }
  }
  if ( $current_page == $last_episode ){
    $classlast = 'off';
    $linklast = '';
  } else {
    $classlast = '';
    if ( $url_pattern == 0 ){
      $linklast = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$last_episode.''._newoption('').'.html';
    } else {
      $linklast = ''.$root.'/'.$lang.'/'.$input_mode.'/'.$content.'__'.$last_episode.'.html';
    }
  }
    echo ''."\n";
    echo '  <div class="readernav col sml-12 sml-centered">'."\n";
    echo '    <div class="col sml-6 med-3 lrg-3 sml-hide lrg-show"><a class="readernavbutton '.$classfirst.'" href="'.$linkfirst.'">&laquo;&nbsp;&nbsp;'._("First").'</a></div> '."\n";
    echo '    <div class="col sml-6 med-6 lrg-3"><a class="readernavbutton '.$classprev.'" href="'.$linkprev.'">&lt; '._("Previous").'</a></div> '."\n";
    echo '    <div class="col sml-6 med-6 lrg-3"><a class="readernavbutton '.$classnext.'" href="'.$linknext.'">'._("Next").' &gt;</a></div> '."\n";
    echo '    <div class="col sml-6 med-3 lrg-3 sml-hide lrg-show"><a class="readernavbutton '.$classlast.'" href="'.$linklast.'">'._("Last").' &raquo;</a></div> '."\n";
    echo '  </div>'."\n";
    echo ''."\n";
}

?>
