<?php
# lib-database.php
# -------------
# Lib-database contains various receipe to scan all episodes, langs and more.
# These loops cost CPU/disk access resources and affect server performances.
# For this reason, this methods here are all cached on disk ('cache/' at root).
# Lib-database returns a set of 4 useful arrays.
# Require a 'cache/' folder at root of website with write permissions.
# Require a database file based, like '0_sources/' of Pepper&Carrot.
# Usage: just include the file on the header of the code.
#
# @author: David Revoy
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher


# Check if we can write in cache/ before starting.
if (!is_writable($cache.'/')) {
  echo '<b style="color: red;font-family:monospace;">Carrot ask write permission to "'.$cache.'/"</b>';
}

# Start with loading the main function
_load_databases();

# Main function:  load the databases if available or ask to regenerate them.
#                 the system listen to renderfarm changes via $sources/last_updated.txt'
function _load_databases() {

  global $debugmode;
  global $sources;
  global $cache;
  global $option;
  global $languages_info;
  global $isolang;
  global $languages_available;
  global $website_translation;
  global $episodes_list;
  global $episodes_all_translations;
  global $all_percent;
  global $po_translation_percent;

  # Put value '1' on index.php at root to display backstage info. 
  if ($debugmode == 3){
    # Delete the tokken: it forces the system to regenerate all each time.
    unlink($cache.'/_last_updated.txt');
  }

  # Prevents an error when comparing last_updated to a non existing file
  if (!file_exists($cache.'/_last_updated.txt')) {
    $cache_placeholder = fopen($cache.'/_last_updated.txt',"w");
    fwrite($cache_placeholder,"placeholder");
    fclose($cache_placeholder);
    chmod($cache.'/_last_updated.txt', 0620);
  }

  # Read the tokken that renderfarm update
  if (file_exists(''.$sources.'/last_updated.txt')) {
    # Compare it to a version we copied previously on our cache
    if (sha1_file(''.$sources.'/last_updated.txt') !== sha1_file($cache.'/_last_updated.txt')) {
      # They differ: regenerate databases
      _generate_databases();
      # Make a new copy of most recent tokken on our cache
      copy(''.$sources.'/last_updated.txt', 'cache/_last_updated.txt');
      chmod($cache.'/_last_updated.txt', 0620);
    }
  }
  
  # Load the databases saved on /cache
  # If the file doesn't exists, recreate them all
  if (file_exists($cache.'/_db_languages_info.txt')) {
    $languages_info = unserialize(file_get_contents($cache.'/_db_languages_info.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_isolang.txt')) {
    $isolang = unserialize(file_get_contents($cache.'/_db_isolang.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_languages_available.txt')) {
    $languages_available = unserialize(file_get_contents($cache.'/_db_languages_available.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_website_translation.txt')) {
    $website_translation = unserialize(file_get_contents($cache.'/_db_website_translation.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_episodes_list.txt')) {
    $episodes_list = unserialize(file_get_contents($cache.'/_db_episodes_list.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_episodes_all_translations.txt')) {
    $episodes_all_translations = unserialize(file_get_contents($cache.'/_db_episodes_all_translations.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_all_percent.txt')) {
    $all_percent = unserialize(file_get_contents($cache.'/_db_all_percent.txt'));
  } else {
    _generate_databases();
  }

  if (file_exists($cache.'/_db_po_translation_percent.txt')) {
    $po_translation_percent = unserialize(file_get_contents($cache.'/_db_po_translation_percent.txt'));
  } else {
    _generate_databases();
  }

  # Put value '2' on index.php at root to display backstage info. 
  if ($debugmode > 1.9){
    _databases_debug();
  }
}

# Generator of databases: the big array are created here and then saved to cache/
function _generate_databases() {

  global $root;
  global $cache;
  global $sources;

  # Database: a convertion of 0_sources/langs.json to PHP array.
  # ============================================================
  # eg. [en] => Array ( [translators] => Array ( [0] => Saffron [1] => Pepper [2] => Carrot )
  #                     [name] => English
  #                     [local_name] => English
  #                     [iso_code] => en
  #                     [iso_version] => 1
  #                     [font] => Array ( [family] => Lavi [size] => 36 ) )
  # Convert the JSON database made by renderfarm to a Php array
  if (file_exists($sources.'/langs.json')) {
    $languages_info = array();
    $languages_info = json_decode(file_get_contents(''.$sources.'/langs.json'), true);
    # Write to disk
    file_put_contents($cache.'/_db_languages_info.txt', serialize($languages_info));
    chmod($cache.'/_db_languages_info.txt', 0620);
  }

  # Database: all languages available.
  # ==================================
  if (file_exists($sources.'/langs.json')) {
    # eg. [0] => ar [1] => bn [2] => br [3] => ca [4] => cn [5] => cs [6] => da [7]
    $languages_available = array();
    # Convert the JSON database made by renderfarm to a Php array
    $lang_database = json_decode(file_get_contents(''.$sources.'/langs.json'), true);
    # Trim the database to only the first key: a list of ISO-639-1-like name
    $languages_available = array_keys($lang_database);
    sort($languages_available);
    # Write to disk
    file_put_contents($cache.'/_db_languages_available.txt', serialize($languages_available));
    chmod($cache.'/_db_languages_available.txt', 0620);
  }

  # Database: a build of the ISO official lang extracted from $languages_info
  # =========================================================================
  # eg. en => en, mx => es_MX, jb => jbo
  $isolang = array();
  # Loop threw all available Pepper&Carrot ISO language
  foreach($languages_available as $language) {
    # Decode the information available for the current language
    $langinfo = $languages_info[$language];

    # Leave empty the lang without iso_code and don't bug for that
    if (!isset($langinfo['iso_code']) || empty($langinfo['iso_code'])) {
      $result = "";
    } else {
      # We have an iso_code, use it:
      $result = $langinfo['iso_code'];
      # We have an iso_script, use it:
      if (isset($langinfo['iso_script']) || !empty($langinfo['iso_script'])) {
        $result .= '_'.$langinfo['iso_script'];
      }
      # We have an iso_locale country code, use it:
      if (isset($langinfo['iso_locale']) || !empty($langinfo['iso_locale'])) {
        $result .= '_'.$langinfo['iso_locale'];
      }
    }
    # Build the array
    # Note: a single liner to build (in $array) "$1" => "$2" 
    # $array[$1]=[$2]
    $isolang[$language] = $result;
  }
  # Write to disk
  file_put_contents($cache.'/_db_isolang.txt', serialize($isolang));
  chmod($cache.'/_db_isolang.txt', 0620);

  # Database: all website translation
  # =================================
  # eg. [0] => br [1] => ca [2] => cn [3] => cs [4] => de [5] => en [6] => eo [7]
  $website_translation = array();
  # Pattern to detect gettext compiled website translation (*.mo files)
  $mo_files = glob('locale/en_US.utf8/LC_MESSAGES/[a-z][a-z].mo');
  # Browse the result
  foreach($mo_files as $mo_file) {
      # Trim the path and keep the two letter ISO-639-1-like name
      $database_entry=basename($mo_file, '.mo');
      # Push it on the table
      array_push($website_translation, $database_entry);
  }
  sort($website_translation);
  # Write to disk
  file_put_contents($cache.'/_db_website_translation.txt', serialize($website_translation));
  chmod($cache.'/_db_website_translation.txt', 0620);


  # Database: all episodes
  # ======================
  # eg.  [0] => ep01_Potion-of-Flight [1] => ep02_Rainbow-potions [2] => ep03_The-secret-ingredients
  $episodes_list = array();
  # Pattern to detect episode folder on 0_sources
  $all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
  # Browse the result
  foreach($all_episodes as $episode) {
    # Trim the path and keep directory name
    $database_entry=basename($episode);
    # Push it on the table
    array_push($episodes_list, $database_entry);
  }
  sort($episodes_list);
  # Write to disk
  file_put_contents($cache.'/_db_episodes_list.txt', serialize($episodes_list));
  chmod($cache.'/_db_episodes_list.txt', 0620);


  # Database: all languages available per episode
  # =============================================
  # eg. [4] => Array ( [0] => br [1] => ca [2] => cn [3] => cs [4] => da )
  $episodes_all_translations = array();
  # Browse episodes
  foreach($episodes_list as $episode) {
    # Open and reset a sub-table at each loop
    $episode_translations = array();
    # Check available translation in directory lang, pick only filtered [a-z][a-z]
    $all_lang_directories = glob(''.$sources.'/'.$episode.'/lang/[a-z][a-z]');
      foreach($all_lang_directories as $lang_directory) {
        # Filter directories only
        if(is_dir($lang_directory)) {
          # Trim the path, keep directory name
          $database_entry = basename($lang_directory);
          # Feed array with list of lang
          array_push($episode_translations, $database_entry);
        }
      }
      # Push for each episodes the sub-table array of lang
      array_push($episodes_all_translations, $episode_translations);
  }
  # Write to disk
  file_put_contents($cache.'/_db_episodes_all_translations.txt', serialize($episodes_all_translations));
  chmod($cache.'/_db_episodes_all_translations.txt', 0620);

  # Database: all percent completion per language
  # =============================================
  # eg. en => 1100 fr => 20 cn => 83
  # Score description: 
  # - 1084 = 84% + website translated
  # - 84 = 84% , website no translated.
  $all_percent = array();
  # Get the number of all episode
  $totalepisodecount = count($episodes_list);
  # Add 1 to $totalepisodecount (website count like for an episode translation)
  $totalepisodecount = $totalepisodecount + 1;
  # Browse language
  foreach($languages_available as $language) {
    # Reset this counter at each loops.
    $score_website = 0;
    $bonus = 0;
    $translationcompletion = 0;
    # Compare if our language is included on the array $website_translation.
    if (in_array($language,$website_translation)){
      $translationcompletion = $translationcompletion + 1;
      # Save the 1000 bonus: if the final score has 4 digit, it will send a signal to the menu the website is translated.
      $bonus = 1000;
    }
    # Browse the list of episode (and get the key)
    foreach($episodes_list as $key => $episode_directory) {
      # Create an array with available lang for the current episode key.
      $lang_for_this_episode = $episodes_all_translations[$key];
      # Compare the current language with the result of this array.
      if(in_array($language,$lang_for_this_episode)) {
        # Increment score when lang is found on this episode.
        $translationcompletion = $translationcompletion + 1;
      }
    }
    # Math to return the percent of completion: 
    $percent = ( $translationcompletion / $totalepisodecount ) * 100;
    # Trim floating results.
    $percent = round($percent, 0);
    # Add the bonus (a leading 1 if website is translated).
    $percent = $percent + $bonus;

    # Feed the array with result
    $all_percent[$language] = $percent;
  }
  # Write to disk
  file_put_contents($cache.'/_db_all_percent.txt', serialize($all_percent));
  chmod($cache.'/_db_all_percent.txt', 0620);

  # Database: all percent of translation completion
  # ===============================================
  # eg. en => 100 fr => 20 cn => 83
  $po_translation_percent = array();
  $path_to_po_percent_json = 'locale/en_US.utf8/LC_MESSAGES/po_translation_percent.json';
  # Convert the JSON database made by renderfarm to a Php array
  if (file_exists($path_to_po_percent_json)) {
    $po_translation_percent = json_decode(file_get_contents($path_to_po_percent_json), true);
  }
  # Write to disk
  file_put_contents($cache.'/_db_po_translation_percent.txt', serialize($po_translation_percent));
  chmod($cache.'/_db_po_translation_percent.txt', 0620);

}

# For testing and debug: print the array of the database
# (you'll have to call it manually by uncomenting its call on previous functions around line 65)
function _databases_debug() {

  global $languages_info;
  global $isolang;
  global $languages_available;
  global $website_translation;
  global $episodes_list;
  global $episodes_all_translations;
  global $all_percent;
  global $po_translation_percent;
  
  echo '<div style="color: #9cbf05; font: 11px monospace; background-color: #2e3436; width: 100%; columns: 100px 3; padding: 20px;">';

  echo '<h2 style="color: #9cbf05;">All the Databases!</h2><br/><br/>';

  echo '<b>[$languages_available]</b>:<br/>';
  print_r($languages_available);
  echo '<br/><br/>';

  echo '<b>[$episodes_list]</b>:<br/>';
  print_r($episodes_list);
  echo '<br/><br/>';

  echo '<b>[$all_percent]</b>:<br/>';
  print_r($all_percent);
  echo '<br/><br/>';

  echo '<b>[$po_translation_percent]</b>:<br/>';
  print_r($po_translation_percent);
  echo '<br/><br/>';

  echo '<b>[$website_translation]</b>:<br/>';
  print_r($website_translation);
  echo '<br/><br/>';

  echo '<b>[$languages_info]</b>:<br/>';
  print_r($languages_info);
  echo '<br/><br/>';

  echo '<b>[$isolang]</b>:<br/>';
  print_r($isolang);
  echo '<br/><br/>';

  echo '<b>[$episodes_all_translations]</b>:<br/>';
  print_r($episodes_all_translations);
  echo '<br/><br/>';

 
  echo '</div>';
}

?>
