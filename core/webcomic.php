<?php if ($root=="") exit;

echo '<div class="containercomic">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

# Resolution directory
# - $option is an array extracted from URL (index.php)
if (in_array("hd", $option)) {
  $resdirectory = "hi-res";
} else {
  $resdirectory = "low-res";
}

# TOOLS
# -----

# Array of all pages
$allpages = glob(''.$sources.'/'.$epdirectory.'/'.$resdirectory.'/'.$lang.''.$credits.'E[0-9][0-9]P[0-9][0-9].*');
if (empty($allpages)) {
  # - fallback to hardcoded English if no translation are available for $lang
  $allpages = glob(''.$sources.'/'.$epdirectory.'/'.$resdirectory.'/en'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
  $fallbackmode = 1;
  $comiclang = 'en';
} else {
  $fallbackmode = 0;
  # In case we fall back to English for an episode that has not been translated yet,
  # Track the comic language in a separate variable
  $comiclang = $lang;
}
sort($allpages);

# Transcript: display the transcript for the current $page
function _transcript($page) {

  global $root;
  global $sources;
  global $comiclang;
  global $option;
  global $content;

  # Extract from $page only E[0-9][0-9]P[0-9][0-9]
  $pageid = basename($page);
  preg_match('/(E[0-9][0-9]P[0-9][0-9])/', $pageid, $matches);
  $pageid = $matches[0];
  # Construct pattern of target transcript file
  $transcriptpage = ''.$sources.'/'.$content.'/hi-res/html/'.$comiclang.'_'.$pageid.'.html';

  # Check if the file exists
  if (file_exists($transcriptpage)) {
    # Check if transcript is requested in URL
    if (in_array("transcript", $option)) {

      # Display transcript html as it is
      readfile($transcriptpage);

    } else {

      # If not requested, show hidden text anyway (for screen readers)
      $transcriptdata = file_get_contents($transcriptpage);
      # Change description list entries into paragraphs to make screen readers less noisy
      $transcript_text = '';
      $lines = explode("\n", $transcriptdata);
      foreach ($lines as $line) {
        $patterns = array(
          '/<dt><strong>(.*)<\/strong><\/dt>/ui',
          '/<dd>(.*)<\/dd>/ui'
        );
          $replacements = array(
          '\\1: ',
          '\\1.<br />'
        );
        $line = preg_replace($patterns, $replacements, trim($line));
        $transcript_text .= $line;
      }
      $transcript_text = trim(preg_replace('/\s*<dl>(.*)<\/dl>\s*/i', '\\1', $transcript_text));
      print('   <div class="hidden">' . $transcript_text . '</div>');
    }
  }
}

$trtitle = ''._("Display the transcript for this episode.").'';

# Hd button special rules
if (in_array("hd", $option)) {
  $hdclass = ' active';
  $gif_width = '2265px';
  $gif_padding = '22px';
} else {
  $hdclass = '';
  $gif_width = '1100px';
  $gif_padding = '32px';
}
# transcript button: highlight management
if (in_array("transcript", $option)) {
  $trclass = ' active';
} else {
  $trclass = '';
}
# Sources button special rules
if (in_array("sources", $option)) {
  $srclass = ' active';
} else {
  $srclass = '';
}
$html_transcript_search = glob(''.$sources.'/'.$epdirectory.'/hi-res/html/'.$comiclang.'_E[0-9][0-9]P00.html');
if (empty($html_transcript_search)) {
  $trtitle = ''._("No transcript available for this episode.").'';
  $trclass = ' off';
}

# Create the index of all episode for the navigation.
$episode_index = array();
$all_episodes = glob(''.$sources.'/ep[0-9][0-9]*');
sort($all_episodes);
foreach ($all_episodes as $key => $episode_directory) {
  $episode_directory = basename($episode_directory);
  array_push($episode_index, $episode_directory);

}
sort($episode_index);

# DISPLAY
# -------

# Display options
echo '  <nav class="col sml-12 sml-text-center">'."\n";
echo '    <a class="switch '.$hdclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("hd").'.html" title="'._("Display the pages of this episode in high resolution.").'">'._("High resolution").'</a>'."\n";
echo '    <a class="switch '.$trclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("transcript").'.html" title="'.$trtitle.'">'._("Transcript").'</a>'."\n";
echo '    <a class="download '.$srclass.'" href="'.$root.'/'.$lang.'/'.$mode.'/'.$content.''._newoption("sources").'.html" title="'._("Display the full set of sources files for this episode in this language.").'">'._("Sources and license").'</a>'."\n";

# Fallback message if translation doesn't exist
if ( $fallbackmode == 1 ){
  echo '    <div class="notification">'._("Oops! There is no translation available yet for this episode with the language you selected. The page will continue in English.").'</div>';
}
echo '  </nav>'."\n";
echo ''."\n";


# Display Sources
if (in_array("sources", $option)) {
  echo '  <div class="col sml-12 sml-text-center" style="margin-top: 28px">'."\n";
  echo '    '._navigation($epdirectory, $episode_index, $mode).'';
  echo '  </div>'."\n";
  echo ''."\n";

  # Include the webcomic sources
  include($file_root.'core/mod-webcomic-sources.php');

# Display the Webcomic episode
} else {

  # Display header
  if (!empty($allpages)) {
    $titlepage = ''.$root.'/'.$allpages[0].'';
    if (file_exists($allpages[0])) {
      echo '  <div class="panel" align="center">'."\n";
      echo '    <img class="comicpage" src="'.$titlepage.'" alt="'._("Header").'" /></a>'."\n";
      echo '    '._transcript($allpages[0]).''."\n";
      echo '  </div>'."\n";
      echo ''."\n";
      # Remove the page from the array
      unset($allpages[0]);
    }


    # Navigation top
    echo ''._navigation($epdirectory, $episode_index, $mode).'';

    # Display the comic pages
    foreach ($allpages as $key => $page) {
      $pagepath = ''.$root.'/'.$page.'';
      if (file_exists($page)) {
        echo '  <article class="panel">'."\n";

        $comic_alt = $header_title.', '._("Page").' '.$key.'';
        $title_alt = ''._("Page").' '.$key.'';

        # Gif: special rule to upscale them
        if (strpos($pagepath, 'gif') == true) {
          echo '    <img class="comicpage" style="max-width:'.$gif_width.';padding-bottom: '.$gif_padding.';" width="92%" src="'.$pagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
        } else {
        # Regular comic page
          echo '    <img class="comicpage" src="'.$pagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
        }

        echo '    '._transcript($page).''."\n";
        echo '  </article>'."\n";
      }
    }
  }
  echo ''."\n";

  # Display a button to show comments
  $episode_number = preg_replace('/[^0-9.]+/', '', $epdirectory);
  $episode_comments = array();
  $episode_comments = json_decode(file_get_contents(''.$sources.'/comments.json'), true);
  $comments_info = $episode_comments['ep'.$episode_number];
  $comments_url = $comments_info["url"];
  $comments_number = $comments_info["nb_com"];

  echo '  <div class="col sml-12 sml-text-center">'."\n";
  echo '    <a class="commentbutton" href="'.$comments_url.'#comments" title="'.
    sprintf(ngettext('Read the %d comment on the blog.', 'Read the %d comments on the blog.', $comments_number),$comments_number).'">'.
    sprintf(ngettext('Read %d comment.', 'Read %d comments.', $comments_number),$comments_number).'</a>';
  echo '  </div>'."\n";

  # Navigation bottom
  echo '<br/>'."\n";
  echo ' '._navigation($epdirectory, $episode_index, $mode).'';
  echo ''."\n";

}

# Container end
echo '</div>'."\n";
echo ''."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo ''."\n";
?>
