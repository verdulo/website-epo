<?php if ($root=="") exit;

include($file_root.'core/lib-credits.php');

# main HTML container:
echo '<section class="col sml-12 med-12 lrg-12 sml-centered" style="max-width: 1480px">';
echo '<div class="grid">';

# List Folder Content
$projectpath = ''.$sources.'/'.$epdirectory;
$foldername = $epdirectory;

function _displayfile($file_path){
  global $root;
  if (file_exists($file_path)) {
    $filename = basename($file_path);
    $filename = str_replace('_by-David-Revoy', '', $filename);
    $fileweight = (filesize($file_path) / 1024) / 1024;
    echo '<a href="'.$root.'/'.$file_path.'" target="_blank" >'.$filename.' <em class="filesize">'.round($fileweight, 2).'MB </em></a><br />';
  } else {
    echo '[bug 404:'.$file_path.']';
  }
}

#Ensure episode exist
if(is_dir($projectpath)) {
  echo '<div class="source col sml-12 med-12 lrg-10 sml-centered">';
  echo '    <div class="col sml-12">';
  echo '      <h1>'.$header_title.'</h1>';
  echo '        '._("Comic pages of Pepper&Carrot comes from two sources: the illustration and the text.").'<br/>';
  echo '        '._("This page offers links to download them, but also to download ready to use compiled rendering.").'<br/><br/>';

  # ***************************************** LEFT COLUMN *******************************************
  echo '    <div class="col sml-12 med-4">';

  # Thumbnail cover
  $episode_number = preg_replace('/[^0-9.]+/', '', $epdirectory);
  $cover_path = ''.$sources.'/'.$epdirectory.'/hi-res/'.$comiclang.''.$credits.'E'.$episode_number.'.jpg';
  if (!file_exists($cover_path)) {
    $cover_path = ''.$sources.'/'.$epdirectory.'/hi-res/en'.$credits.'E'.$episode_number.'.jpg';
  }
  echo '      <div class="imgfill">'."\n";
  echo '        '._img($root.'/'.$cover_path, _("Cover of the episode"), 405, 300, 88).'<br/>'."\n";
  echo '        <br/>';
  echo '      </div>'."\n";


  # Git folders
  # -----------
  $gitlab_path = 'https://framagit.org/peppercarrot/webcomics/-/tree/master/'.$epdirectory.'/lang/'.$comiclang.'';
  $history_path = 'https://framagit.org/peppercarrot/webcomics/-/commits/master/'.$epdirectory.'/lang/'.$comiclang.'';
  echo '      <div style="padding:0 15px 15px 30px">'."\n";
  echo '        <img style="float:left; margin-right:10px; margin-top:5px;" src="'.$root.'/core/img/git.svg" alt=""/> '."\n";
  echo '        <a href="'.$gitlab_path.'" target="_blank" >'._("Git directory").'</a><br />'."\n";
  echo '        <a href="'.$history_path.'" target="_blank" >'._("Git history").'</a><br />'."\n";
  echo '      </div>'."\n";

  # License
  # -------
  $goodpracticelink = ''.$root.'/'.$lang.'/documentation/120_License_best_practices.html';
  echo '      <div class="ccbox">';
  echo '        <strong>'._("License:").'</strong><br/>';
  echo '        <img src="'.$root.'/core/img/ccby.jpg" style="margin-top: 10px;"/><br/>';
  echo '        '._("If you republish this episode, you need to provide the following information:").'<br/>';
  echo '        <br/><a href="https://creativecommons.org/licenses/by/4.0/">'._("Creative Commons Attribution 4.0 International license").'</a><br/>';
  echo '        '._("Attribution to:").'';
  echo '        '._print_credits($comiclang, $epdirectory);
  echo '        <br/>'._("Credit for the universe of Pepper&Carrot, Hereva:").'<br/>';
  echo '        '._print_hereva();
  echo '        '._("Note: these credits are different depending the episode selected and the language.").' ';
  echo '        '.sprintf(_("More information and good practice for attribution can be found <a href=\"%s\">on the documentation</a>."),$goodpracticelink).'<br/>';
  # TODO: Add a link to documentation
  echo '      </div>';

  # Single-page
  echo '      <br/>'._("Collage of all pages into a single image file").':<br/>';
  $singlepage_path = ''.$sources.'/'.$epdirectory.'/low-res/single-page/'.$comiclang.''.$credits.'E'.$episode_number.'XXL.jpg';
  _displayfile($singlepage_path);

  echo '      <br/>';
  echo '    </div>';

  # ***************************************** RIGHT COLUMN *******************************************
  echo '    <div class="col pad sml-12 med-8">';

  # ##### TOP BIG BUTTONS ########
  # KRITA SOURCE PACK (Self hosted)
  $artwork_sources = ''.$sources.'/'.$epdirectory.'/zip/'.$epdirectory.'_art-pack.zip';
  if (file_exists($artwork_sources)){
    echo '    <div class="buttonsource">'."\n";
    echo '      <img style="float:left; margin-right:10px; margin-top:5px;" src="'.$root.'/core/img/paint.svg" alt=""/> '."\n";
    echo '      '._("<strong>Illustration sources</strong>: Krita (kra) pages.").''."\n";
    echo '      <br/>'."\n";
    $filename = basename($artwork_sources);
    $fileweight = (filesize($artwork_sources) / 1024) / 1024;
    echo '      <a href="'.$root.'/'.$artwork_sources.'" target="_blank" >'.$filename.' <em class="filesize" style="color:#444;">'.round($fileweight, 2).'MB </em></a><br />'."\n";
    echo '    </div>'."\n";
  }

  # LANG PACK (Self hosted)
  $lang_sources = ''.$sources.'/'.$epdirectory.'/zip/'.$epdirectory.'_lang-pack.zip';
  if (file_exists($lang_sources)){
    echo '    <div class="buttonsource">'."\n";
    echo '      <img style="float:left; margin-right:10px; margin-top:5px;" src="'.$root.'/core/img/lang.svg" alt=""/> '."\n";
    echo '      '._("<strong>Text sources:</strong> Inkscape (svg) pages.").''."\n";
    echo '      <br/>'."\n";
    $filename = basename($lang_sources);
    $fileweight = (filesize($lang_sources) / 1024) / 1024;
    echo '      <a href="'.$root.'/'.$lang_sources.'" target="_blank" >'.$filename.' <em class="filesize" style="color:#444;">'.round($fileweight, 2).'MB </em></a><br />'."\n";
    echo '    </div>'."\n";
  }

  echo '<div class="grid">';

  # Ready to use compiled renderings:
  # ---------------------------------
  echo '<div class="col pad sml-12"><br/>';
  # we scan all the valid pattern pages inside episode folder
  $pattern = ''.$sources.'/'.$epdirectory.'/low-res/'.$comiclang.'*P[0-9][0-9].jpg';
  $search = glob($pattern);
  if (!empty($search)){
    echo '&nbsp;<strong>'._("Ready to use compiled renderings:").'</strong><br/>';
    echo '&nbsp;'._("High quality and recommended for printing.<br/><br/>");
    echo '  <div style="clear:both"></div>'."\n";

    # Perform a cut/paste of the header title at the end of array
    $copy_header_path = $search[0];
    unset($search[0]);
    array_push($search,$copy_header_path);
    # Input the cover at the end
    $cover_path = str_replace('P00.jpg', '.jpg', $copy_header_path);
    array_push($search,$cover_path);
    # Standard position in our array are named
    $cover_page = array_key_last($search);
    $header_page = $cover_page - 1;
    $credit_page = $cover_page - 2;

    foreach ($search as $key => $filepath) {
      # extracting from the path the filename and path itself
      $filename = basename($filepath);
      $fullpath = dirname($filepath);
      # define
      $hires_path = ''.$root.'/'.$projectpath.'/hi-res/'.$filename.'';
      $filenamegfx = str_replace($comiclang.'_', 'gfx_', $filename);
      $gfx_path = ''.$root.'/'.$projectpath.'/hi-res/gfx-only/'.$filenamegfx.'" title="'.$header_title.'';
      $filenametxt = str_replace('.jpg', '.png', $filename);
      $txtonly_path = ''.$root.'/'.$projectpath.'/hi-res/txt-only/'.$filenametxt.'';
      # Check path and disk access.
      if (file_exists($filepath)) {
        # Adapt captions to the type of page
        $caption = _("Page").' '.$key.'';
        if ( $key === $credit_page ) {
          $caption = _("Credits");
        }
        if ( $key === $header_page ) {
          $caption = _("Header");
        }
        if ( $key === $cover_page ) {
          $caption = _("Cover");
        }
        $title = $caption.', '._("click to enlarge.");
        # Display the thumbnails with captions
        echo '<figure class="thumbnail col sml-6 med-4 lrg-3"><a href="'.$hires_path.'" title="'.$header_title.'" target="_blank">'."\n";
        # Put the img into a sized div for holding the room in case of differently sized pages (eg. gif, header)
        echo '    '._img($root.'/'.$filepath, $title, 210, 270, 88).''."\n";
        # Keep this line around to test payload of the page without thumbnail generation, easier to blame resources monsters.
        #echo '  <img src="'.$root.'/'.$filepath.'" title="'.$title.'" title="'.$title.'">';
        echo '</a><figcaption class="sourcescaptions text-center">'."\n";
        echo '  <strong>'.$caption.'</strong><br>'."\n";
        echo '  <a href="'.$hires_path.'" target="_blank">'._("Illustration and text").'</a><br>'."\n";
        echo '  <a href="'.$gfx_path.'" target="_blank">'._("Only illustration").'</a><br>'."\n";
        echo '  <a href="'.$txtonly_path.'" target="_blank">'._("Only text").'</a><br>'."\n";
        echo '</figure>'."\n";
      }
    }
  }
  # Propose also the gif for episode with gif animation:
  $pattern = ''.$sources.'/'.$epdirectory.'/low-res/'.$comiclang.'*P[0-9][0-9].gif';
  $search = glob($pattern);
  if (!empty($search)){
    foreach ($search as $key => $filepath) {
      # extracting from the path the filename and path itself
      $filename = basename($filepath);
      $fullpath = dirname($filepath);
      $caption = _("Gif");
      $title = $caption.', '._("click to enlarge.");
      $gif_path = ''.$root.'/'.$projectpath.'/low-res/'.$filename.'';
      $filenamepng = str_replace('.gif', '.png', $filename);
      $filenamegfx = str_replace($comiclang.'_', 'gfx_', $filenamepng);
      $gifstatic_path = ''.$root.'/'.$projectpath.'/hi-res/gfx-only/lossless/'.$filenamegfx.'';
      echo '<figure class="thumbnail col sml-6 med-4 lrg-3"><a href="'.$gif_path.'" title="'.$header_title.'" target="_blank">'."\n";
      echo '  <img src="'.$root.'/'.$filepath.'" title="'.$title.'" title="'.$title.'">';
      echo '</a><figcaption class="sourcescaptions text-center">'."\n";
      echo '  <strong>'.$caption.'</strong><br>'."\n";
      echo '  <a href="'.$gif_path.'" target="_blank">'._("Animation").'</a><br>'."\n";
      echo '  <a href="'.$gifstatic_path.'" target="_blank">'._("Static version for print").'</a><br>'."\n";
      echo '</figure>'."\n";
    }
  }

  echo '</div>';
  echo '</div>';

  echo '</div>';
  echo '</div>';
  echo '</div>';
  echo '</div>';
  echo '</article>';
}

?>
