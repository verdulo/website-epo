# Pepper&Carrot website sources

A rewrite from scratch made in 2021 of the 'legacy website'(2014 to 2021).

![Pepper&Carrot header image](core/img/2021-cover-lrg.jpg)

## Contributions

### Translation

<a href="https://weblate.framasoft.org/engage/peppercarrot/">
<img src="https://weblate.framasoft.org/widgets/peppercarrot/-/svg-badge.svg" alt="Translation status" />
</a> <a href="https://weblate.framasoft.org/engage/peppercarrot/">
<img src="https://weblate.framasoft.org/widgets/peppercarrot/-/multi-auto.svg" alt="Translation status" />
</a>

Merge requests about translation, correction, proofreading are all very welcome. The translation uses Gettext and PO files. All PO files and the POT template catalog are stored in the [po directory](po). They use the Pepper&Carrot two letter language code registered in the [webcomics langs JSON file ](https://framagit.org/peppercarrot/webcomics/-/blob/master/langs.json). The title artwork ``po/en.svg`` is used in many places and you can also translate this one with your own art for the title.

Credits about website contributions can edited in ``/po/en-credits.json`` (eg. for English 'en') and can be read online under the License tab of the website. You can check the [info.json documentation](https://www.peppercarrot.com/en/documentation/081_info.json.html) about how to fill them.

For more details on translating, see the [translation documentation on the website](https://www.peppercarrot.com/en/documentation/110_Translate_the_website.html). Translation maintenance is documented in [po/README.md](po/README.md).

We are currently working on setting up a [weblate](https://weblate.framasoft.org/projects/peppercarrot/) project. This is still a work in progress, so don't translate there yet.

### Code

Merge request about little corrections (CSS, Php) are often welcome. Big rewrite or features that require many lines of codes might be rejected: I need to be able to re-read the code base anytime to fix and maintain the website, and my limited coding skill often made it difficult for me to re-read the logic of the code of other. Credits about code contribution can be edited here https://framagit.org/peppercarrot/webcomics/-/blob/master/project-global-credits.json and can be read online under the License tab of the website.

## Install

### Preparing repo

The website will not run as you can see on www.peppercarrot.com just after a fresh clone and running it in a PHP environment. It require a file based database in 0_sources and data and a temporary cache folder. This repository comes with a "dummy database" with example files (not the true one that weight 98GB as measured when I write this lines). But for this database to not collide with the real one; I renamed the folder (I placed a dot in front of them; so they are hidden).

To deploy the dummy database, do:

```
cp -r .0_sources 0_sources
cp -r .data data
cp .gitignore.template .gitignore
cp -r .0_sources/0ther/artworks 0_sources/0ther/misc
cp -r .0_sources/0ther/artworks 0_sources/0ther/sketchbook
cp -r .0_sources/0ther/artworks 0_sources/0ther/book-publishing
mkdir -p cache
chmod 777 cache
touch 0_sources/last_updated.txt
chmod 666 0_sources/last_updated.txt
```

After that, you'll have a result similar to the testing version of the website: https://www.test.peppercarrot.com

### Hosting or running

Hosting this website requires PHP, GD image library, XML support for PHP, URL rewrite, JSON, ZIP. The database is made of flat-file, no MySQL. For a local install on a Ubuntu machine, I usually do it this way if my file are part of my home folder ~/peppercarrot/website :

```
sudo apt install apache2
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf
sudo a2enconf fqdn
sudo a2enmod rewrite
sudo apt install php libapache2-mod-php php-gd php-xml php-mbstring
sudo systemctl restart apache2
sudo rm -r /var/www/html
sudo ln -s ~/peppercarrot/website /var/www/html
```
and for permissions
```
sudo gedit /etc/apache2/sites-available/000-default.conf
```
add this at the end of this file
```
<Directory "/var/www/html">
  AllowOverride All
</Directory>
```
and finish with a
```
sudo systemctl restart apache2
```
Finally, copy `configure.php.new` to `configure.php` and make any changes you need for your server setup.

You can use your browser to visit http://localhost/ and the site should run. In case you use sub-folder; you can configure the root of the website on index.php.

Enjoy!

## License

See the file LICENSE file in this repository or visit [GNU GPL V3](http://www.gnu.org/copyleft/gpl.html).
