This guide is aimed at GitLab repository & Weblate maintainers. For a translator's guide, please check the [documentation on the website](https://www.peppercarrot.com/gd/documentation/110_Translate_the_website.html) itself.

* [How to translate the website](#how-to-translate-the-website)
* [Adding a new language](#adding-a-new-language)
* [How to maintain the translations](#how-to-maintain-the-translations)
  1. [Updating the .pot reference catalog](#1-source-updating-the-pot-reference-catalog)
  2. [Synchronizing translations with the reference catalog](#2-source-synchronizing-translations-with-the-reference-catalog)
  3. [Compiling the translations for use by the website](#3-website-compiling-the-translations-for-use-by-the-website)
  4. [Updating translation statistics for use by the website](#4-website-updating-translation-statistics-for-use-by-the-website)
* [General Weblate Configuration](https://framagit.org/peppercarrot/website-2021/-/blob/10-weblate-documentation/po/README.md#general-weblate-configuration)
  * [Translation Instructions & Code of Conduct](#translation-instructions-code-of-conduct)
  * [GitLab Integration](#gitlab-integration)


# How to translate the website

The translation consists of 3 parts:

1. A `<locale>-credits.json` file listing the translators for your language
2. A `<locale>.po` file with the actual translation source code.
   This will be compiled by a script into the `locale` directory.
3. Optionally, a `<locale>.svg` file with the header image

For further guidance, please refer to the [documentation on the website](https://www.peppercarrot.com/en/documentation/110_Translate_the_website.html).

# Adding a new language

Since we use non-standard locale codes for some languages, we need to map the codes
to the official ISO codes in weblate.

1. From the Poject's weblate main page, visit Manage → Settings
   → [Workflow](https://weblate.framasoft.org/settings/peppercarrot/#workflow).
   ![Settings](doc/weblate-main-settings.png)
2. Add the locale to the list in **Language aliases**. It's a `,`-separated list,
   and each item has the format `<ISO locale>:<custom locale>`.
   ![Aliases](doc/weblate-language-aliases.png)
3. Add the `<custom locale>.po` file and merge to `main` – weblate will then pick it up automatically.
4. To get the new file up straight away, visit Manage
   → [Repository Maintenance](https://weblate.framasoft.org/projects/peppercarrot/#repository).
   Use the "Update" or the "Force Synchronization" option.
   ![Repository Maintenance Screen](doc/weblate-repository-maintenance.png)

# How to maintain the translations

This directory has 4 scripts in it for maintaining the translations. The first 2 scripts need to be run on the Git repository when the source code changes. The last 2 scripts need to be run on the webserver when updates come in from translators.

**Before changing any of the existing `pot`/`po` files, lock the Weblate repository to prevent merge conflicts**. You can do that at Manage
→ [Repository Maintenance](https://weblate.framasoft.org/projects/peppercarrot/#repository):

1. Lock
2. Push
3. Merge on GitLab
4. Update files in GitLab
5. Red "Maintenance" button → "Force synchronization"
6. Red "Maintenance" button → "Reset" (this is why we locked it). This will get rid of commits that only contain date updates and meaningless line break differences.
7. Unlock

The weblate repository is at https://weblate.framasoft.org/git/peppercarrot/website/ and you can add this as a git remote in case you need to do some emergency fixes on the command line. It acts like any Git fork, but we can only push changes to it via the Weblate Maintenance UI.

## 1. Source: Updating the .pot reference catalog

`extract_strings_from_PHP_and_save_to_catalog-pot.sh`: Updates the `_catalog.pot` file.
   Run this whenever there's a change in the website's source code that changes a translatable string.

## 2. Source: Synchronizing translations with the reference catalog

Weblate will automatically create merge requests with translation changes for us.
For manual updates, trigger a merge request from Weblate first
to avoid merge conflicts if the same translation has been touched.

1. Go to Manage → [Repository Maintenance](https://weblate.framasoft.org/projects/peppercarrot/#repository) on Weblate
2. Click the `Commit` button
3. Click the `Push` button
4. Go to [Merge Requests](https://framagit.org/peppercarrot/website-2021/-/merge_requests) on GitLab. There should be a new Merge Request called "Translations update from Weblate". Merge it into `main`.
5. Run the following script and commit it to the `main` branch:

`update_all_po_from_catalog-pot.sh`: Updates all `<locale>.po` files with the
information from `_catalog.pot`, so that translators can start working.
Note that every tool uses different line breaks in this file format,
so be careful about not creating merge conflicts.

## 3. Website: Compiling the translations for use by the website

`compile_all_PO_into_MO.sh`: Turns `<locale>.po` translation source files into
binary `.mo` files and places them under the `locale` directory.
Run this script on the live installation whenever there's a translation update.
Do not commit the `.mo` files into git.

## 4. Website: Updating translation statistics for use by the website

`extract_translation_percent_completion.sh`: Creates a file `all-po-translation-percentage.json` that is placed under the `locale` directory.

# General Weblate Configuration

This is stuff we don't normally touch, just for reference in case the infrastructure changes.

## Translation Instructions & Code of Conduct

* Translation Instructions: https://weblate.framasoft.org/settings/peppercarrot/
* Code of Conduct: https://weblate.framasoft.org/settings/peppercarrot/website/ (This is set separately for each component)

## GitLab Integration

Weblate is based on Git and acts like a fork of the Pepper & Carrot website that only touches the translation files.
The integration between Weblate and GitLab needs some settings in both systems.

### On GitLab

#### Notifying Weblate of Changes

We need to configure GitLab so it will notify Weblate about any changes to the `main` branch.
This is done via Webhooks.
* [GitLab Documentation](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
* [Weblate Documentation](https://docs.weblate.org/en/weblate-4.6.2/admin/continuous.html#automatically-receiving-changes-from-gitlab)

First, get the *personal API key* from Weblate and copy it to the clipboard.
You can get it from Manage → [API Access](https://weblate.framasoft.org/access/peppercarrot/#api).

![GitLab Webhook Settings](doc/weblate-api-access.png)

On the Pepper & Carrot website GitLab, go to Settings → [Webhooks](https://framagit.org/peppercarrot/website-2021/-/hooks).

1. Set **URL** to the weblate project's hook address (`https://weblate.framasoft.org/hooks/gitlab/`)
2. Paste the *personal API key* from Weblate into the **Secret token** field
3. Select **Push events** and set the branch to `main`
4. Scroll down and click **Add webhook**

![GitLab Webhook Settings](doc/gitlab-webhook.png)

The new webhook will appear on the bottom of the page.
It has a test function where you can trigger it to see if Weblate is picking it up.
You can check the log on Weblate in the Manage
→ [Repository Maintenance](https://weblate.framasoft.org/projects/peppercarrot/#repository) section.

#### Receiving Translation updates from Weblate

Framasoft have set up a special [Weblate user on GitLab](https://framagit.org/weblate) that automatically creates merge requests from changes in the translation on Weblate. So, no extra settings are needed on the GitLab side for pushing from Weblate to GitLab.

### On Weblate

To configure Weblate:

1. Go to the Pepper & Carrot / Website component
2. Choose Manage → Settings → [Version Control](https://weblate.framasoft.org/settings/peppercarrot/website/#vcs)
3. Use the following settings:

| Name | Value | Purpose |
| ------ | ------ | ------ |
| Version control system | `GitLab` | This method will create Merge Requests for us, so it's more convenient than using `Git` |
| Source code repository | `git@framagit.org:peppercarrot/website-2021.git` | The repository address that Weblate uses to find the translation files |
| Repository branch | `main` | The branch that Weblate uses to pull changes from |
| Repository push URL | `git@framagit.org:peppercarrot/website-2021.git` | Target fork for the automated Merge Requests created by the [Weblate user](https://framagit.org/weblate) that contain the changes from Weblate |
| Push branch  |  | Leave this empty for the `GitLab` method, because we're getting Merge Requests from the Weblate fork instead |
| Repository browser | `https://framagit.org/peppercarrot/website-2021/-/blob/{{branch}}/core/{{filename\|parentdir}}#L{{line}}` | With this setting, translators can find the string to translate in the source code with a simple mouse click ([Documentation](https://docs.weblate.org/en/weblate-4.6.2/admin/projects.html#component-repoweb)) |
| Push on commit | ✅ | We want automated merge requests |
| Age of changes to commit | `24` | How often merge requests are created when there is a change |
| Merge style | `Rebase` or `Merge` | Default is `Rebase` and I just left it alone
| Lock on error | ✅ | Leaving this on will make sure that translators stop working when there's a problem |


![Weblate Version Control Settings](doc/weblate-version-control.png)

[Weblate documentation](https://docs.weblate.org/en/weblate-4.6.2/admin/continuous.html#push-changes)
